# Rotfang Foreign Data Wrapper for PostgreSQL

This FDW generates random data for its tables. The tables are readonly.

Certain data types are supported natively. User supplied functions are also
supported which can supply random data for an unsupported data type, or can
override the inbuilt functions. The user supplied function is specified with the
func_colname option where colname is the name of the column the function
is to be used for. 

Current natively supported data types are:

- boolean
- text
- smallint
- integer
- bigint
- double precision
- bytea

Support for other types will be added soon.

Random null values are also generated for 10% of the values for fields marked as nullable.

Example use:

    CREATE EXTENSION rotfang_fdw;
    
    CREATE FOREIGN TABLE rand1 (f1 boolean, f2 text, f3 integer)
    SERVER rotfang;

    SELECT * FROM rand1 LIMIT 20;

    ALTER FOREIGN TABLE rand1 OPTIONS (ADD maxrows '1000');

    CREATE FUNCTION random_tstz (typmod int)
    RETURNS timestamptz
    LANGUAGE SQL as
    $$
        SELECT now() - (random() * 20.0) * interval '1 year'
    $$;
    
    CREATE FOREIGN TABLE rand2 (b boolean, ts timestamptz)
    SERVER rotfang
    OPTIONS (maxrows '10', func_ts 'random_tstz');

    SELECT * FROM rand2;

Why Rotfang?
------------ 

Because it's shorter than any sane name for the module.

    The Rotfang Conspiracy is [...] working to bring down the Ministry of 
    Magic from within using a combination of Dark Magic and gum disease.

       -- Luna Lovegood in Harry Potter and the Half-Blood Prince

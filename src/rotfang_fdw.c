/*-------------------------------------------------------------------------
 *
 * Rotfang Foreign Data Wrapper for PostgreSQL
 *
 * Copyright (c) 2013 Andrew Dunstan
 *
 * This software is released under the PostgreSQL Licence
 *
 * Author: Andrew Dunstan <andrew@dunslane.net>
 *
 * IDENTIFICATION
 *		  rotfang_fdw/src/rotfang_fdw.c
 *
 *-------------------------------------------------------------------------
 */

#include "postgres.h"

#include "funcapi.h"
#include "miscadmin.h"
#include "access/htup_details.h"
#include "access/reloptions.h"
#include "catalog/pg_type.h"
#include "catalog/pg_foreign_table.h"
#include "commands/defrem.h"
#include "foreign/fdwapi.h"
#include "foreign/foreign.h"
#include "optimizer/pathnode.h"
#include "optimizer/planmain.h"
#include "optimizer/restrictinfo.h"
#include "utils/builtins.h"
#include "utils/rel.h"

#include <math.h>

#if PG_VERSION_NUM < 90400
#error only Postgres 9.4 and greater is supported
#endif

PG_MODULE_MAGIC;

/*
 * SQL functions
 */
extern Datum rotfang_fdw_handler(PG_FUNCTION_ARGS);
extern Datum rotfang_fdw_validator(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(rotfang_fdw_handler);
PG_FUNCTION_INFO_V1(rotfang_fdw_validator);


/* callback functions */
static void rotfangGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid);

static void rotfangGetForeignPaths(PlannerInfo *root,
						 RelOptInfo *baserel,
						 Oid foreigntableid);

static ForeignScan *rotfangGetForeignPlan(PlannerInfo *root,
						RelOptInfo *baserel,
						Oid foreigntableid,
						ForeignPath *best_path,
						List *tlist,
						List *scan_clauses
#if (PG_VERSION_NUM >= 90500)
					   ,Plan *outer_plan
#endif
	);

static void rotfangBeginForeignScan(ForeignScanState *node,
						  int eflags);

static TupleTableSlot *rotfangIterateForeignScan(ForeignScanState *node);

static void rotfangReScanForeignScan(ForeignScanState *node);

static void rotfangEndForeignScan(ForeignScanState *node);
static void rotfangExplainForeignScan(ForeignScanState *node,
									  struct ExplainState * es);



/* random gen functions */
static Datum getrandombool(void);
static Datum getrandombytea(void);
static Datum getrandomint(int size);
static Datum getrandomtext(void);
static Datum getrandomfloat(int size);

/*
 * structures used by the FDW
 *
 */

/*
 * Describes the valid options for objects that use this wrapper.
 */
struct rotfangFdwOption
{
	const char *optname;
	Oid			optcontext;		/* Oid of catalog in which option may appear */
};


typedef struct {
	char *colname;
	char *funcname;
} rotfangColFunc;

typedef struct {
	int maxrows;
	rotfangColFunc *colfuncs;
} rotfangTableOptions;

/*
 * The plan state is set up in rotfangGetForeignRelSize and stashed away in
 * baserel->fdw_private and fetched in rotfangGetForeignPaths and
 * rotfangGetForeignPlan.
 */
typedef struct
{
	char	   *foo;
	int			bar;
} RotfangFdwPlanState;

/*
 * The scan state is for maintaining state for a scan, eiher for a
 * SELECT or UPDATE or DELETE.
 *
 * It is set up in rotfangBeginForeignScan and stashed in node->fdw_state
 * and subsequently used in rotfangIterateForeignScan,
 * rotfangEndForeignScan, rotfangReScanForeignScan. and
 * rotfangExplainForeignScan.
 */
typedef struct
{
	Relation   rel;
	AttInMetadata  *attinmeta;
    int        attr_count;
	int        n_rows;
	int        maxrows;
	FmgrInfo  *p_flinfo;
} RotfangFdwScanState;

/*
 * The modify state is for maintaining state of modify operations.
 *
 * It is set up in rotfangBeginForeignModify and stashed in
 * rinfo->ri_FdwState and subsequently used in rotfangExecForeignInsert,
 * rotfangExecForeignUpdate, rotfangExecForeignDelete and
 * rotfangEndForeignModify.
 */
typedef struct
{
	char	   *chimp;
	int			chump;
} RotfangFdwModifyState;


Datum
rotfang_fdw_handler(PG_FUNCTION_ARGS)
{
	FdwRoutine *fdwroutine = makeNode(FdwRoutine);

	elog(DEBUG1, "entering function %s", __func__);

	/*
	 * assign the handlers for the FDW
	 */

	fdwroutine->GetForeignRelSize = rotfangGetForeignRelSize;
	fdwroutine->GetForeignPaths = rotfangGetForeignPaths;
	fdwroutine->GetForeignPlan = rotfangGetForeignPlan;
	fdwroutine->BeginForeignScan = rotfangBeginForeignScan;
	fdwroutine->IterateForeignScan = rotfangIterateForeignScan;
	fdwroutine->ReScanForeignScan = rotfangReScanForeignScan;
	fdwroutine->EndForeignScan = rotfangEndForeignScan;
	fdwroutine->ExplainForeignScan = rotfangExplainForeignScan;

	PG_RETURN_POINTER(fdwroutine);
}

Datum
rotfang_fdw_validator(PG_FUNCTION_ARGS)
{
	List	   *options_list = untransformRelOptions(PG_GETARG_DATUM(0));
	Oid         catalog = PG_GETARG_OID(1);

    ListCell   *cell;

	int maxrows = 0;

	elog(DEBUG1, "entering function %s", __func__);

	/* make sure the options are valid */

    foreach(cell, options_list)
    {
        DefElem    *def = (DefElem *) lfirst(cell);

		if (strcmp(def->defname, "maxrows") == 0)
        {
            if (maxrows)
                ereport(ERROR,
                        (errcode(ERRCODE_SYNTAX_ERROR),
                         errmsg("conflicting or redundant options: maxrows (%s)",
                                defGetString(def))
                         ));

            maxrows = atoi(defGetString(def));
			if (maxrows <= 0 || maxrows > 1000000)
                ereport(ERROR,
                        (errcode(ERRCODE_SYNTAX_ERROR),
                         errmsg("maxrows (%s) out of range 1 to 1000000",
                                defGetString(def))
                         ));
        }
		else if (catalog == ForeignTableRelationId && 
				 strncmp(def->defname, "func_",5) == 0)
		{
			/* 
			 * can't do much more here - 
			 * do more checking when we get to begin foreign plan 
			 */
		}
		else
			ereport(ERROR,
					(errcode(ERRCODE_FDW_INVALID_OPTION_NAME),
					 errmsg("invalid option in this context %s", def->defname),
					 errhint("Rotfang FDW does not support this option")));
	}

	PG_RETURN_VOID();
}

#if (PG_VERSION_NUM >= 90200)
static void
rotfangGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid)
{
	/*
	 * Obtain relation size estimates for a foreign table. This is called at
	 * the beginning of planning for a query that scans a foreign table. root
	 * is the planner's global information about the query; baserel is the
	 * planner's information about this table; and foreigntableid is the
	 * pg_class OID of the foreign table. (foreigntableid could be obtained
	 * from the planner data structures, but it's passed explicitly to save
	 * effort.)
	 *
	 * This function should update baserel->rows to be the expected number of
	 * rows returned by the table scan, after accounting for the filtering
	 * done by the restriction quals. The initial value of baserel->rows is
	 * just a constant default estimate, which should be replaced if at all
	 * possible. The function may also choose to update baserel->width if it
	 * can compute a better estimate of the average result row width.
	 */

	RotfangFdwPlanState *plan_state;

	elog(DEBUG1, "entering function %s", __func__);

	baserel->rows = 0;

	plan_state = palloc0(sizeof(RotfangFdwPlanState));
	baserel->fdw_private = (void *) plan_state;

	/* initialize required state in plan_state */

}

static void
rotfangGetForeignPaths(PlannerInfo *root,
					   RelOptInfo *baserel,
					   Oid foreigntableid)
{
	/*
	 * Create possible access paths for a scan on a foreign table. This is
	 * called during query planning. The parameters are the same as for
	 * GetForeignRelSize, which has already been called.
	 *
	 * This function must generate at least one access path (ForeignPath node)
	 * for a scan on the foreign table and must call add_path to add each such
	 * path to baserel->pathlist. It's recommended to use
	 * create_foreignscan_path to build the ForeignPath nodes. The function
	 * can generate multiple access paths, e.g., a path which has valid
	 * pathkeys to represent a pre-sorted result. Each access path must
	 * contain cost estimates, and can contain any FDW-private information
	 * that is needed to identify the specific scan method intended.
	 */

	RotfangFdwPlanState *plan_state = baserel->fdw_private;
	
	Cost		startup_cost,
				total_cost;

	elog(DEBUG1, "entering function %s", __func__);

	Assert(plan_state != NULL);

	startup_cost = 0;
	total_cost = startup_cost + baserel->rows;

	/* Create a ForeignPath node and add it as only possible path */
	add_path(baserel, (Path *)
			 create_foreignscan_path(root, baserel,
#if (PG_VERSION_NUM >= 90600)
                                     NULL,      /* default pathtarget */
#endif
									 baserel->rows,
									 startup_cost,
									 total_cost,
									 NIL,		/* no pathkeys */
									 NULL,		/* no outer rel either */
#if (PG_VERSION_NUM >= 90500)
                                     NULL,      /* no extra plan */
#endif
									 NIL));		/* no fdw_private data */
}



static ForeignScan *
rotfangGetForeignPlan(PlannerInfo *root,
					  RelOptInfo *baserel,
					  Oid foreigntableid,
					  ForeignPath *best_path,
					  List *tlist,
					  List *scan_clauses
#if (PG_VERSION_NUM >= 90500)
					   ,Plan *outer_plan
#endif
	)
{
	/*
	 * Create a ForeignScan plan node from the selected foreign access path.
	 * This is called at the end of query planning. The parameters are as for
	 * GetForeignRelSize, plus the selected ForeignPath (previously produced
	 * by GetForeignPaths), the target list to be emitted by the plan node,
	 * and the restriction clauses to be enforced by the plan node.
	 *
	 * This function must create and return a ForeignScan plan node; it's
	 * recommended to use make_foreignscan to build the ForeignScan node.
	 *
	 */

	RotfangFdwPlanState *plan_state = baserel->fdw_private;

	Index		scan_relid = baserel->relid;

	/*
	 * We have no native ability to evaluate restriction clauses, so we just
	 * put all the scan_clauses into the plan node's qual list for the
	 * executor to check. So all we have to do here is strip RestrictInfo
	 * nodes from the clauses and ignore pseudoconstants (which will be
	 * handled elsewhere).
	 */

	elog(DEBUG1, "entering function %s", __func__);

	Assert(plan_state != NULL);

	scan_clauses = extract_actual_clauses(scan_clauses, false);

	/* Create the ForeignScan node */
#if(PG_VERSION_NUM < 90500)
	return make_foreignscan(tlist,
							scan_clauses,
							scan_relid,
							NIL,	/* no expressions to evaluate */
							NIL);		/* no private state either */
#else
    return make_foreignscan(tlist,
                            scan_clauses,
                            scan_relid,
                            NIL,    /* no expressions to evaluate */
                            NIL,    /* no private state either */
                            NIL,    /* no custom tlist */
                            NIL,    /* no remote quals */
                            outer_plan);
#endif

}

#endif


static void
rotfangGetTableOptions(RotfangFdwScanState *scan_state)
{
	Oid tableid = RelationGetRelid(scan_state->rel);

    ForeignTable *table;
    ForeignServer *server;
    UserMapping *mapping;
    List       *options;
    ListCell   *lc;
	
	TupleDesc tupDesc = RelationGetDescr(scan_state->rel);
	Form_pg_attribute *attr = tupDesc->attrs;
	

    table = GetForeignTable(tableid);
	server = GetForeignServer(table->serverid);
    mapping = GetUserMapping(GetUserId(), table->serverid);
	
    options = NIL;
    options = list_concat(options, table->options);
    options = list_concat(options, server->options);
    options = list_concat(options, mapping->options);

    /* Loop through the options */
    foreach(lc, options)
    {
        DefElem    *def = (DefElem *) lfirst(lc);

        if (strcmp(def->defname, "maxrows") == 0)
            scan_state->maxrows = atoi(defGetString(def));
		else if (strncmp(def->defname, "func_",5) == 0)
		{
			char *colname = def->defname + 5;
			Oid funcoid;
			int i;
			for (i = 0; i < scan_state->attr_count; i++)
			{
				if (strcmp(colname, attr[i]->attname.data) != 0)
					continue;
				funcoid = DirectFunctionCall1(to_regproc, CStringGetDatum(defGetString(def)));
				if (funcoid == InvalidOid)
					elog(ERROR,"unknown func %s", defGetString(def));
				fmgr_info(funcoid, &(scan_state->p_flinfo[i]));
			}
		}
	}

	if (scan_state->maxrows == 0)
		scan_state->maxrows = 1000000;
}

static void
rotfangBeginForeignScan(ForeignScanState *node,
						int eflags)
{
	/*
	 * Begin executing a foreign scan. This is called during executor startup.
	 * It should perform any initialization needed before the scan can start,
	 * but not start executing the actual scan (that should be done upon the
	 * first call to IterateForeignScan). The ForeignScanState node has
	 * already been created, but its fdw_state field is still NULL.
	 * Information about the table to scan is accessible through the
	 * ForeignScanState node (in particular, from the underlying ForeignScan
	 * plan node, which contains any FDW-private information provided by
	 * GetForeignPlan). eflags contains flag bits describing the executor's
	 * operating mode for this plan node.
	 *
	 * Note that when (eflags & EXEC_FLAG_EXPLAIN_ONLY) is true, this function
	 * should not perform any externally-visible actions; it should only do
	 * the minimum required to make the node state valid for
	 * ExplainForeignScan and EndForeignScan.
	 *
	 */

	RotfangFdwScanState *scan_state = palloc0(sizeof(RotfangFdwScanState));

	Relation rel = node->ss.ss_currentRelation;

    TupleDesc       tupDesc;
    int         attr_count;

	elog(DEBUG1, "entering function %s", __func__);

	node->fdw_state = scan_state;

    tupDesc = RelationGetDescr(rel);
    attr_count  = tupDesc->natts;

	scan_state->p_flinfo = (FmgrInfo *) palloc0(sizeof(FmgrInfo) * attr_count);

	scan_state->attr_count = attr_count;

	scan_state->rel = rel;

    scan_state->attinmeta = TupleDescGetAttInMetadata(RelationGetDescr(scan_state->rel));

	scan_state->n_rows = 0;

	rotfangGetTableOptions(scan_state);

}


static TupleTableSlot *
rotfangIterateForeignScan(ForeignScanState *node)
{
	/*
	 * Fetch one row from the foreign source, returning it in a tuple table
	 * slot (the node's ScanTupleSlot should be used for this purpose). Return
	 * NULL if no more rows are available. The tuple table slot infrastructure
	 * allows either a physical or virtual tuple to be returned; in most cases
	 * the latter choice is preferable from a performance standpoint. Note
	 * that this is called in a short-lived memory context that will be reset
	 * between invocations. Create a memory context in BeginForeignScan if you
	 * need longer-lived storage, or use the es_query_cxt of the node's
	 * EState.
	 *
	 * The rows returned must match the column signature of the foreign table
	 * being scanned. If you choose to optimize away fetching columns that are
	 * not needed, you should insert nulls in those column positions.
	 *
	 * Note that PostgreSQL's executor doesn't care whether the rows returned
	 * violate any NOT NULL constraints that were defined on the foreign table
	 * columns — but the planner does care, and may optimize queries
	 * incorrectly if NULL values are present in a column declared not to
	 * contain them. If a NULL value is encountered when the user has declared
	 * that none should be present, it may be appropriate to raise an error
	 * (just as you would need to do in the case of a data type mismatch).
	 */


	RotfangFdwScanState *scan_state = (RotfangFdwScanState *) node->fdw_state;

	TupleTableSlot *slot = node->ss.ss_ScanTupleSlot;

	HeapTuple   tuple;

	Assert(scan_state != NULL);

	elog(DEBUG1, "entering function %s", __func__);

	/* get the next record, if any, and fill in the slot */

	ExecClearTuple(slot);

	if (scan_state->n_rows < scan_state->maxrows)
	{
		Datum *values;
		bool  *nulls;
		int i;

		TupleDesc tupDesc = RelationGetDescr(scan_state->rel);
		Form_pg_attribute *attr = tupDesc->attrs;

		values = palloc0(scan_state->attr_count * sizeof(Datum));
		nulls = palloc0(scan_state->attr_count * sizeof(bool));

		for (i = 0; i < scan_state->attr_count; i++)
		{
			if (attr[i]->attisdropped)
				continue;

			if (!attr[i]->attnotnull && (random() % 10) == 0)
			{
				/* use an arbitrary 10% of null values */
				nulls[i] = true;
			}
			else if (scan_state->p_flinfo[i].fn_addr != NULL)
			{
				values[i] = FunctionCall1(&(scan_state->p_flinfo[i]), Int32GetDatum(attr[i]->atttypmod));
			}
			else 
			{
				switch (attr[i]->atttypid)
				{
					case BOOLOID:
						values[i] = getrandombool();
						break;
					case BYTEAOID:
						values[i] = getrandombytea();
						break;
					case INT8OID:
						values[i] = getrandomint(8);
						break;
					case INT4OID:
						values[i] = getrandomint(4);
						break;
					case INT2OID:
						values[i] = getrandomint(2);
						break;
					case TEXTOID:
						values[i] = getrandomtext();
						break;
					case FLOAT8OID:
						values[i] = getrandomfloat(8);
						break;
					case FLOAT4OID:
						values[i] = getrandomfloat(4);
						break;
					case BPCHAROID:
					case VARCHAROID:
					case DATEOID:
					case TIMEOID:
					case TIMESTAMPOID:
					case TIMESTAMPTZOID:
					case NUMERICOID:
					default:
						elog(ERROR,
							 "random data for type %u not yet supported",
							 attr[i]->atttypid);
				} /* switch */
			} /* else */
		} /* for attr */

		tuple = heap_form_tuple(tupDesc, values, nulls);
		
		ExecStoreTuple(tuple, slot, InvalidBuffer, false);
	}

	scan_state->n_rows++;

	/* then return the slot */
	return slot;
}


static void
rotfangReScanForeignScan(ForeignScanState *node)
{
	/*
	 * Restart the scan from the beginning. Note that any parameters the scan
	 * depends on may have changed value, so the new scan does not necessarily
	 * return exactly the same rows.
	 */

	RotfangFdwScanState *scan_state = (RotfangFdwScanState *) node->fdw_state;

	Assert(scan_state != NULL);

	elog(DEBUG1, "entering function %s", __func__);

}


static void
rotfangEndForeignScan(ForeignScanState *node)
{
	/*
	 * End the scan and release resources. It is normally not important to
	 * release palloc'd memory, but for example open files and connections to
	 * remote servers should be cleaned up.
	 */

	RotfangFdwScanState *scan_state = (RotfangFdwScanState *) node->fdw_state;

	Assert(scan_state != NULL);

	elog(DEBUG1, "entering function %s", __func__);

}

static void
rotfangExplainForeignScan(ForeignScanState *node,
							struct ExplainState * es)
{
	/*
	 * Print additional EXPLAIN output for a foreign table scan. This function
	 * can call ExplainPropertyText and related functions to add fields to the
	 * EXPLAIN output. The flag fields in es can be used to determine what to
	 * print, and the state of the ForeignScanState node can be inspected to
	 * provide run-time statistics in the EXPLAIN ANALYZE case.
	 *
	 * If the ExplainForeignScan pointer is set to NULL, no additional
	 * information is printed during EXPLAIN.
	 */

	RotfangFdwScanState *scan_state = (RotfangFdwScanState *) node->fdw_state;

	Assert(scan_state != NULL);

	elog(DEBUG1, "entering function %s", __func__);

}



static Datum 
getrandombool(void)
{
	PG_RETURN_BOOL(random() % 2 == 1);
}

static Datum 
getrandombytea(void)
{
	char buff[256];

	int len = (int)(rint(255));
	int i;

	for (i = 0; i < len; i++)
		buff[i] = 32 + (int)(rint(96));

	buff[len] = '\0';
	
	
	PG_RETURN_TEXT_P(CStringGetTextDatum(buff));
}

static Datum 
getrandomint(int size)
{
	int64 res = random();

	int neg = random() %2;
	if (neg == 1)
		res = - res;

	if (size == 8) 
		PG_RETURN_INT64((int64)res);
	else if (size == 4) 
		PG_RETURN_INT32((int32)res);
	else /*  (size == 2)  */
		PG_RETURN_INT16((int16)res);
}

static Datum 
getrandomtext(void)
{
	char buff[256];

	int len;
	int i;

	len = random() % 256;

	for (i = 0; i < len; i++)
		buff[i] = 32 + random() % 96;

	buff[len] = '\0';
	
	
	PG_RETURN_TEXT_P(CStringGetTextDatum(buff));
}

static Datum 
getrandomfloat(int size)
{
	double x = drand48() * (mrand48() % 100000000L);
	if (size == 4)
		PG_RETURN_FLOAT4((float4) x);

	PG_RETURN_FLOAT8(x);
}

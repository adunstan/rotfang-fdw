/*-------------------------------------------------------------------------
 *
 *                foreign-data wrapper  rotfang
 *
 * Copyright (c) 2015, Andrew Dunstan
 *
 * This software is released under the PostgreSQL Licence
 *
 * Author:  Andrew Dunstan <andrew@dunslane.net>
 *
 * IDENTIFICATION
 *                rotfang_fdw/=sql/rotfang_fdw.sql
 *
 *-------------------------------------------------------------------------
 */

CREATE FUNCTION rotfang_fdw_handler()
RETURNS fdw_handler
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION rotfang_fdw_validator(text[], oid)
RETURNS void
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FOREIGN DATA WRAPPER rotfang_fdw
  HANDLER rotfang_fdw_handler
  VALIDATOR rotfang_fdw_validator;

CREATE SERVER rotfang FOREIGN DATA WRAPPER rotfang_fdw;

CREATE USER MAPPING FOR PUBLIC SERVER rotfang;

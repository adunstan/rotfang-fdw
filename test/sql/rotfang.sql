CREATE FOREIGN TABLE rand1 (f1 boolean, f2 text, f3 integer)
SERVER rotfang;

SELECT * FROM rand1 WHERE false LIMIT 1;

ALTER FOREIGN TABLE rand1 OPTIONS (ADD maxrows '100');

CREATE FUNCTION random_tstz (typmod int)
RETURNS timestamptz
LANGUAGE SQL as
$$
	SELECT now() - (random() * 20.0) * interval '1 year'
$$;

CREATE FOREIGN TABLE rand2 (b boolean, ts timestamptz)
SERVER rotfang
OPTIONS (maxrows '1000', func_ts 'random_tstz');

SELECT * FROM rand2 WHERE false LIMIT 1;

SELECT count(*) FROM rand2;
